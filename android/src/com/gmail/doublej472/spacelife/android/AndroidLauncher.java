package com.gmail.doublej472.spacelife.android;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.gmail.doublej472.spacelife.SpaceLifeGame;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		config.numSamples = 4;
		config.hideStatusBar = true;
		config.useAccelerometer = false;
		config.useCompass = false;
		config.useWakelock = true;

		initialize(new SpaceLifeGame(), config);
	}
}

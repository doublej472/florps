package com.gmail.doublej472.spacelife.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.gmail.doublej472.spacelife.SpaceLifeGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();

		config.setWindowedMode(1280, 720);
		config.setResizable(true);
		config.setTitle("Florps!");
		config.useVsync(true);

		new Lwjgl3Application(new SpaceLifeGame(), config);
	}
}

package com.gmail.doublej472.spacelife;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gmail.doublej472.spacelife.states.IntroState;
import com.gmail.doublej472.spacelife.states.State;

public class SpaceLifeGame extends ApplicationAdapter {

	private static int level = 1;
	private static boolean hq = true;
	private static boolean musicOn = true;

	private static boolean dirtyData = false;
	private static Viewport vp;
	private static State currentState;
	private static SoundManager sm;
	private Preferences prefs;
	private SpriteBatch sb;
	private BitmapFont font;

	public static Viewport getVp() {
		return vp;
	}

	public static void switchState(State state) {
		currentState.dispose();

		// Clean up garbage between state changes
		System.gc();

		currentState = state;
	}

	public static SoundManager getSoundManager() {
		return sm;
	}

	private static boolean dataDirty() {
		return dirtyData;
	}

	public static boolean isHq() {
		return hq;
	}

	public static void setHq(boolean hq) {
		SpaceLifeGame.hq = hq;
		dirtyData = true;
	}

	public static boolean isMusicOn() {
		return musicOn;
	}

	public static void setMusicOn(boolean musicOn) {
		SpaceLifeGame.musicOn = musicOn;
		if (musicOn) {
			getSoundManager().music.setVolume(0.31f);
		} else {
			getSoundManager().music.setVolume(0f);
		}
		dirtyData = true;
	}


	public static int getLevel() {
		return level;
	}

	public static void setLevel(int level) {
		SpaceLifeGame.level = level;
		dirtyData = true;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		vp.update(width, height);
	}

	@Override
	public void create () {
		prefs = Gdx.app.getPreferences("Florps Preferences");

		sb = new SpriteBatch();

		sm = new SoundManager();

		vp = new FitViewport(16, 9);

		currentState = new IntroState();

		// Generate font for the menu
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = 16;
		parameter.color = com.badlogic.gdx.graphics.Color.WHITE;
		parameter.borderColor = com.badlogic.gdx.graphics.Color.BLACK;
		parameter.borderWidth = 2;
		font = generator.generateFont(parameter);
		generator.dispose();

		// Load saved data
		loadData();
	}

	private void loadData() {
		level = prefs.getInteger("level", 1);
		hq = prefs.getBoolean("hq", true);
		musicOn = prefs.getBoolean("musicOn", true);
	}

	@Override
	public void render () {
		currentState.tick(Gdx.graphics.getDeltaTime());
		currentState.draw();

		if (dataDirty()) {
			saveData();
		}

		//sb.begin();
		//font.draw(sb, "FPS: " + Gdx.graphics.getFramesPerSecond(), 0, 16);
		//sb.end();
	}

	public void saveData() {
		prefs.putInteger("level", level);
		prefs.putBoolean("hq", isHq());
		prefs.putBoolean("musicOn", isMusicOn());
		prefs.flush();
	}
}

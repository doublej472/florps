package com.gmail.doublej472.spacelife;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.gmail.doublej472.spacelife.entities.*;

/**
 * Created by doublej472 on 12/20/15.
 */
public class EntityManager {

	private final Array<Entity> activeEntities = new Array<Entity>();

	public void tick(float dt) {
		for (Entity e : activeEntities) {
			e.tick(dt);
		}

		for (Entity e : CollisionListener.toRemove) {
			e.destroy();
			activeEntities.removeValue(e, true);
			CollisionListener.toRemove.removeValue(e, true);
		}
	}

	public void newFood(Vector2 position) {
		FoodEntity e = new FoodEntity(position);
		activeEntities.add(e);
	}

	public void newRock(Vector2 position) {
		RockEntity e = new RockEntity(position);
		activeEntities.add(e);
	}

	public void newMadFlorp(Vector2 pos, PlayerEntity player) {
		MadFlorpEntity e = new MadFlorpEntity(pos, player);
		activeEntities.add(e);
	}

	public void newCharger(Vector2 pos, PlayerEntity player) {
		ChargerEntity e = new ChargerEntity(pos, player);
		activeEntities.add(e);
	}

	public void dispose() {
		activeEntities.clear();
	}

	public void draw() {
		for (Entity e : activeEntities) {
			e.draw();
		}
	}

	public int foodNumber() {
		int num = 0;
		for (Entity e : activeEntities) {
			if (e instanceof FoodEntity) {
				num += 1;
			}
		}
		return num;
	}

	public void newHomeRock(Vector2 position) {
		HomeRockEntity e = new HomeRockEntity(position);
		activeEntities.add(e);
	}

	public void killEnemies() {
		for (Entity e : activeEntities) {
			if (e instanceof MadFlorpEntity || e instanceof ChargerEntity) {
				activeEntities.removeValue(e, true);
				CollisionListener.toRemove.add(e);
			}
		}
	}

	public void newTimePowerup(Vector2 position) {
		TimePowerupEntity e = new TimePowerupEntity(position);
		activeEntities.add(e);
	}

	public void newInvincibilityPowerup(Vector2 position) {
		InvincibilityEntity e = new InvincibilityEntity(position);
		activeEntities.add(e);
	}

	public int powerupNumber() {
		int num = 0;
		for (Entity e : activeEntities) {
			if (e instanceof TimePowerupEntity || e instanceof InvincibilityEntity) {
				num += 1;
			}
		}
		return num;
	}

	public HomeRockEntity getClosestHome(Vector2 position) {
		HomeRockEntity h = null;
		float dist = Float.MAX_VALUE;

		for (Entity e : activeEntities) {
			if (e instanceof HomeRockEntity && e.getPosition().dst(position) < dist) {
				dist = e.getPosition().dst(position);
				h = (HomeRockEntity) e;
			}
		}

		return h;
	}
}

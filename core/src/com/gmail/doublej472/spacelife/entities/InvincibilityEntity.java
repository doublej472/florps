package com.gmail.doublej472.spacelife.entities;

import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.gmail.doublej472.spacelife.states.GameState;

/**
 * Created by doublej472 on 1/5/16.
 */
public class InvincibilityEntity extends Entity {

	private static final float RADIUS = 0.25f;

	private final PointLight light;

	private Color col1 = new Color(1, 1, 0, 1);
	private Color col2 = new Color(1, 0, 0, 1);
	private Color col3 = new Color(0, 1, 0, 1);

	private Color targetCol1 = new Color(1, 1, 0, 1);
	private Color targetCol2 = new Color(1, 0, 0, 1);
	private Color targetCol3 = new Color(0, 1, 0, 1);

	public InvincibilityEntity(Vector2 pos) {
		super(pos);

		//CircleShape shape = new CircleShape();
		//shape.setRadius(RADIUS / 2);

		Vector2[] verts = new Vector2[3];
		verts[0] = new Vector2(RADIUS, 0);
		verts[1] = new Vector2(RADIUS, 0).rotate(120);
		verts[2] = new Vector2(RADIUS, 0).rotate(240);

		PolygonShape shape = new PolygonShape();
		shape.set(verts);

		FixtureDef fDef = new FixtureDef();
		fDef.restitution = 0.5f;
		fDef.friction = 0.1f;
		fDef.density = 10;
		fDef.shape = shape;

		body.createFixture(fDef);

		light = new PointLight(GameState.rayHandler, 16, new Color(0.5f, 1, 1, 0.65f), 5, getPosition().x, getPosition().y);
		light.setXray(true);
		light.attachToBody(body);
	}

	@Override
	public void tick(float dt) {
		if (col1.equals(targetCol1)) {
			targetCol1.set((float) (Math.random() * 0.5 + 0.5), (float) (Math.random()), (float) (Math.random() * 0.5 + 0.5), 1);
		}
		if (col2.equals(targetCol2)) {
			targetCol2.set((float) (Math.random() * 0.5 + 0.5), (float) (Math.random()), (float) (Math.random() * 0.5 + 0.5), 1);
		}
		if (col3.equals(targetCol3)) {
			targetCol3.set((float) (Math.random() * 0.5 + 0.5), (float) (Math.random()), (float) (Math.random() * 0.5 + 0.5), 1);
		}

		col1.lerp(targetCol1, dt);
		col2.lerp(targetCol2, dt);
		col3.lerp(targetCol3, dt);
	}

	@Override
	public void draw() {
		ShapeRenderer sr = GameState.getShapeRenderer();

		Vector2 pos = getPosition();

		sr.setColor(Color.WHITE);

		sr.triangle(pos.x + RADIUS * 1.05f * (float) (Math.cos(body.getAngle())), pos.y + RADIUS * 1.05f * (float) (Math.sin(body.getAngle())),
				pos.x + RADIUS * 1.05f * (float) (Math.cos(body.getAngle() + 2 * Math.PI / 3)), pos.y + RADIUS * 1.05f * (float) (Math.sin(body.getAngle() + 2 * Math.PI / 3)),
				pos.x + RADIUS * 1.05f * (float) (Math.cos(body.getAngle() + 4 * Math.PI / 3)), pos.y + RADIUS * 1.05f * (float) (Math.sin(body.getAngle() + 4 * Math.PI / 3)));

		sr.triangle(pos.x + RADIUS * (float) (Math.cos(body.getAngle())), pos.y + RADIUS * (float) (Math.sin(body.getAngle())),
				pos.x + RADIUS * (float) (Math.cos(body.getAngle() + 2 * Math.PI / 3)), pos.y + RADIUS * (float) (Math.sin(body.getAngle() + 2 * Math.PI / 3)),
				pos.x + RADIUS * (float) (Math.cos(body.getAngle() + 4 * Math.PI / 3)), pos.y + RADIUS * (float) (Math.sin(body.getAngle() + 4 * Math.PI / 3)),
				col1, col2, col3);
	}
}

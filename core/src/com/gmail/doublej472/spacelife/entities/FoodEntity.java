package com.gmail.doublej472.spacelife.entities;

import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.gmail.doublej472.spacelife.states.GameState;

/**
 * Created by doublej472 on 12/20/15.
 *
 * This is the food Entity, nothing too special here.
 * Can be eaten, can be pushed around.
 */
public class FoodEntity extends Entity {

	private final float foodRadius = (float) Math.random() * 0.1f + 0.2f;

	private PointLight light;

	public FoodEntity (Vector2 position) {
		super(position);

		CircleShape shape = new CircleShape();
		shape.setRadius(foodRadius);

		FixtureDef fDef = new FixtureDef();
		fDef.restitution = 0.5f;
		fDef.friction = 0.1f;
		fDef.density = 10;
		fDef.shape = shape;

		body.createFixture(fDef);

		light = new PointLight(GameState.rayHandler, 16, new Color(0, 1, 0, 0.5f), 3 + foodRadius * 2, getPosition().x, getPosition().y);
		light.setXray(true);
		light.attachToBody(body);
	}

	@Override
	public void tick(float dt) {
	}

	@Override
	public void draw() {
		ShapeRenderer sr = GameState.getShapeRenderer();

		sr.setColor(0, 1, 0, 1);
		sr.circle(body.getPosition().x, body.getPosition().y, foodRadius, 25);
		sr.setColor(0.5f, 1, 0.5f, 1);
		sr.circle(body.getPosition().x, body.getPosition().y, foodRadius * 0.75f, 20);
	}
}

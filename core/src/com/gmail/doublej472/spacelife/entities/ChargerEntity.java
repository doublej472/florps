package com.gmail.doublej472.spacelife.entities;

import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.gmail.doublej472.spacelife.states.GameState;

/**
 * Created by doublej472 on 12/23/15.
 */
public class ChargerEntity extends Entity {

	public static final float CHARGER_SIDE = 1f;
	private static final float MAX_COUNTDOWN = 1f;
	private final PointLight light;
	private Color bodyColor = new Color();
	private PlayerEntity player;
	private float countDown = MAX_COUNTDOWN;

	public ChargerEntity(Vector2 position, PlayerEntity player) {
		super(position);

		this.player = player;

		bodyColor.set(1.0f, 0.25f, 0f, 1);

		PolygonShape shape = new PolygonShape();
		shape.setAsBox(CHARGER_SIDE / 2f, CHARGER_SIDE / 2f);

		FixtureDef fDef = new FixtureDef();
		fDef.restitution = 0.2f;
		fDef.friction = 0.1f;
		fDef.density = 500;
		fDef.shape = shape;

		body.createFixture(fDef);

		light = new PointLight(GameState.rayHandler, 16, new Color(1f, 0, 0, 1), 10, getPosition().x, getPosition().y);
		light.setXray(true);
		light.attachToBody(body);

	}

	@Override
	public void tick(float dt) {
		countDown -= dt;
		bodyColor.set(1, MAX_COUNTDOWN - countDown, countDown / MAX_COUNTDOWN, 1);
		if (countDown <= 0) {
			countDown = (float) (MAX_COUNTDOWN + (Math.random() - 0.5)*0.1);

			// Start with a magnitude 250000 newton(!) force to begin with
			Vector2 force = new Vector2(250000, 0);

			// get the player coords
			Vector2 pos = player.getPosition();

			// set the force angle to point to the player
			force.setAngleRad((float) Math.atan2(pos.y - body.getPosition().y, pos.x - body.getPosition().x));

			// Move the Charger
			body.applyForceToCenter(force, true);

			// Rotate haphazardly
			body.applyTorque((float) (100000f * (Math.random() - 0.5f)), true);
		}
	}

	@Override
	public void draw() {
		ShapeRenderer sr = GameState.getShapeRenderer();

		Vector2 pos = player.getPosition();

		sr.setColor(Color.BLACK);
		sr.rect(body.getPosition().x - CHARGER_SIDE / 2f, body.getPosition().y - CHARGER_SIDE / 2f,
				CHARGER_SIDE / 2f, CHARGER_SIDE / 2f, CHARGER_SIDE, CHARGER_SIDE, 1.05f, 1.05f, ((float) Math.toDegrees(body.getAngle())));

		sr.setColor(bodyColor);
		sr.rect(body.getPosition().x - CHARGER_SIDE / 2f, body.getPosition().y - CHARGER_SIDE / 2f,
				CHARGER_SIDE / 2f, CHARGER_SIDE / 2f, CHARGER_SIDE, CHARGER_SIDE, 1f, 1f, ((float) Math.toDegrees(body.getAngle())));

		sr.setColor(Color.BLACK);

		sr.circle(body.getPosition().x,
				body.getPosition().y, CHARGER_SIDE * 0.5f, 25);

		sr.setColor(Color.WHITE);

		sr.circle(body.getPosition().x,
				body.getPosition().y, CHARGER_SIDE * 0.45f, 25);

		sr.setColor(Color.RED);

		float eyeAngle = (float) Math.atan2(pos.y - (body.getPosition().y), pos.x - (body.getPosition().x));

		float eyeRadius = CHARGER_SIDE * 0.15f;

		sr.circle(body.getPosition().x + (float) Math.cos(eyeAngle) * (0.45f - eyeRadius),
				body.getPosition().y + (float) Math.sin(eyeAngle) * (0.45f - eyeRadius), eyeRadius, 15);
	}
}

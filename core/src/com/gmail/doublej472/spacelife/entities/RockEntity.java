package com.gmail.doublej472.spacelife.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.gmail.doublej472.spacelife.states.GameState;

/**
 * Created by doublej472 on 12/20/15.
 */
public class RockEntity extends Entity {

	private float rockRadius = (float) Math.random() * 7f + 2f;

	public RockEntity(Vector2 position) {
		super(position);

		CircleShape shape = new CircleShape();
		shape.setRadius(rockRadius);

		FixtureDef fDef = new FixtureDef();
		fDef.restitution = 0.1f;
		fDef.friction = 0.35f;
		fDef.density = 100;
		fDef.shape = shape;

		body.createFixture(fDef);

	}

	@Override
	public void tick(float dt) {

	}

	@Override
	public void draw() {
		ShapeRenderer sr = GameState.getShapeRenderer();

		sr.setColor(Color.GRAY);

		sr.circle(body.getPosition().x, body.getPosition().y, rockRadius, 100);
		sr.setColor(Color.DARK_GRAY);
		sr.circle(body.getPosition().x, body.getPosition().y, rockRadius * 0.5f, 50);
	}
}

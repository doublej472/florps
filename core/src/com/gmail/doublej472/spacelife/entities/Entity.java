package com.gmail.doublej472.spacelife.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.gmail.doublej472.spacelife.states.GameState;

/**
 * Created by doublej472 on 12/19/15.
 */
public abstract class Entity {

	protected Body body;

	public Entity(Vector2 position) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.linearDamping = 0.8f;
		bodyDef.angularDamping = 0.8f;
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.angle = (float) Math.random() * (float) Math.PI * 2;
		bodyDef.position.set(position);

		body = GameState.getWorld().createBody(bodyDef);
		body.setUserData(this);
	}

	public abstract void tick(float dt);

	public abstract void draw();

	public void destroy() {
		GameState.getWorld().destroyBody(body);
	}

	public Vector2 getPosition() {
		return body.getPosition();
	}

	public Body getBody() {
		return body;
	}
}

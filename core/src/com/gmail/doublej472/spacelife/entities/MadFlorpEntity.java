package com.gmail.doublej472.spacelife.entities;

import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.gmail.doublej472.spacelife.states.GameState;

/**
 * Created by doublej472 on 12/22/15.
 */
public class MadFlorpEntity extends Entity {

	public static final float FLORP_RADIUS = 0.5f;
	private PointLight light;

	private Color bodyColor = new Color();
	private PlayerEntity player;

	public MadFlorpEntity(Vector2 position, PlayerEntity player) {
		super(position);

		this.player = player;

		bodyColor.set(1.0f, 0.25f, 0f, 1);

		CircleShape shape = new CircleShape();
		shape.setRadius(FLORP_RADIUS);

		FixtureDef fDef = new FixtureDef();
		fDef.restitution = 0.5f;
		fDef.friction = 0.1f;
		fDef.density = 100;
		fDef.shape = shape;

		body.createFixture(fDef);

		light = new PointLight(GameState.rayHandler, 16, new Color(1f, 0, 0, 1), 10, getPosition().x, getPosition().y);
		light.setXray(true);
		light.attachToBody(body);

	}

	@Override
	public void tick(float dt) {
		// Start with a magnitude 450 newton force to begin with
		Vector2 force = new Vector2(450, 0);

		// get the player coords
		Vector2 pos = player.getPosition();

		// set the force angle to point to the player
		force.setAngleRad((float) Math.atan2(pos.y - body.getPosition().y, pos.x - body.getPosition().x));

		// Move the Florp
		body.applyForceToCenter(force, true);

		float angle1 = force.angleRad();
		float angle2 = body.getAngle();

		// Fix angles to the 0...2PI range
		while (angle1 > Math.PI * 2f) {
			angle1 -= Math.PI * 2f;
		}

		while (angle1 < 0) {
			angle1 += Math.PI * 2f;
		}

		while (angle2 > Math.PI * 2f) {
			angle2 -= Math.PI * 2f;
		}

		while (angle2 < 0) {
			angle2 += Math.PI * 2f;
		}

		float angle;

		// Give the angular velocity depending on the difference
		// of angles wanted and have
		// angle1 = angle wanted
		// angle2 = angle have
		if (angle1 > angle2 + Math.PI) {
			angle = (angle1 - angle2) - (float) Math.PI * 2f;
		} else if (angle1 < angle2 - Math.PI) {
			angle = (angle1 - angle2) + (float) Math.PI * 2f;
		} else {
			angle = (angle1 - angle2);
		}

		// Set a max angular velocity
		if (angle > Math.PI / 4) {
			angle = (float) Math.PI / 4;
		} else if (angle < -Math.PI / 4) {
			angle = (float) -Math.PI / 4;
		}

		// Apply angular velocity
		if (Math.abs(body.getAngularVelocity()) < 2f) {
			body.applyTorque(angle * 80f, true);
		}
	}

	@Override
	public void draw() {
		ShapeRenderer sr = GameState.getShapeRenderer();

		Vector2 pos = player.getPosition();

		float angle = (float) Math.atan2(pos.y - body.getPosition().y, pos.x - body.getPosition().x);

		sr.setColor(bodyColor);
		sr.circle(body.getPosition().x, body.getPosition().y, FLORP_RADIUS, 50);

		sr.setColor(Color.BLACK);

		sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f,
				body.getPosition().y + (float) Math.sin(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f, FLORP_RADIUS / 4f, 25);

		sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f,
				body.getPosition().y + (float) Math.sin(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f, FLORP_RADIUS / 4f, 25);

		sr.setColor(Color.RED);

		float eyeAngle1 = (float) Math.atan2(pos.y - (body.getPosition().y + (float) Math.sin(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f),
				pos.x - (body.getPosition().x + (float) Math.cos(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f));
		float eyeAngle2 = (float) Math.atan2(pos.y - (body.getPosition().y + (float) Math.sin(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f),
				pos.x - (body.getPosition().x + (float) Math.cos(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f));

		// Give it jittery eyes
		eyeAngle1 += (Math.random() - 0.5) * 0.3;
		eyeAngle2 += (Math.random() - 0.5) * 0.3;

		float eyeRadius = FLORP_RADIUS / 10f;

		sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f + (float) Math.cos(eyeAngle1) * (eyeRadius),
				body.getPosition().y + (float) Math.sin(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f + (float) Math.sin(eyeAngle1) * (eyeRadius), eyeRadius, 15);

		sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f + (float) Math.cos(eyeAngle2) * (eyeRadius),
				body.getPosition().y + (float) Math.sin(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f + (float) Math.sin(eyeAngle2) * (eyeRadius), eyeRadius, 15);
	}
}

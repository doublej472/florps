package com.gmail.doublej472.spacelife.entities;

import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.gmail.doublej472.spacelife.states.GameState;

/**
 * Created by doublej472 on 1/5/16.
 */
public class TimePowerupEntity extends Entity {

	private static final float RADIUS = 0.25f;

	private final PointLight light;

	public TimePowerupEntity(Vector2 pos) {
		super(pos);

		CircleShape shape = new CircleShape();
		shape.setRadius(RADIUS);

		FixtureDef fDef = new FixtureDef();
		fDef.restitution = 0.5f;
		fDef.friction = 0.1f;
		fDef.density = 10;
		fDef.shape = shape;

		body.createFixture(fDef);

		light = new PointLight(GameState.rayHandler, 16, new Color(0.5f, 1, 1, 0.65f), 5, getPosition().x, getPosition().y);
		light.setXray(true);
		light.attachToBody(body);
	}

	@Override
	public void tick(float dt) {

	}

	@Override
	public void draw() {
		ShapeRenderer sr = GameState.getShapeRenderer();

		sr.setColor(0, 0, 0, 1);
		sr.circle(body.getPosition().x, body.getPosition().y, RADIUS, 25);
		sr.setColor(1, 1, 1, 1);
		sr.circle(body.getPosition().x, body.getPosition().y, RADIUS * 0.9f, 20);

		sr.setColor(0, 0, 0, 1);
		sr.rectLine(getPosition().x, getPosition().y, getPosition().x + (float) Math.cos(getBody().getAngle())*RADIUS*0.8f,
					getPosition().y + (float) Math.sin(getBody().getAngle())*RADIUS*0.8f, 0.02f);

		sr.rectLine(getPosition().x, getPosition().y, getPosition().x + (float) Math.cos(getBody().getAngle() + Math.PI*0.75)*RADIUS*0.6f,
					getPosition().y + (float) Math.sin(getBody().getAngle() + Math.PI*0.75)*RADIUS*0.6f, 0.02f);
	}
}

package com.gmail.doublej472.spacelife.entities;

import box2dLight.PointLight;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.gmail.doublej472.spacelife.SpaceLifeGame;
import com.gmail.doublej472.spacelife.states.GameState;

/**
 * Created by doublej472 on 12/19/15.
 */
public class PlayerEntity extends Entity {

	public static final float FLORP_RADIUS = 0.5f;

	private Color bodyColor = new Color();

	private float health = 10f;
	private float food = 0f;

	private boolean dead = false;

	private PointLight light;

	public PlayerEntity(Vector2 position) {
		super(position);

		body.setAngularDamping(1.5f);

		bodyColor.set(0, 0.5f, 0.9f, 1);

		CircleShape shape = new CircleShape();
		shape.setRadius(FLORP_RADIUS);

		FixtureDef fDef = new FixtureDef();
		fDef.restitution = 0.5f;
		fDef.friction = 0.1f;
		fDef.density = 100;
		fDef.shape = shape;

		body.createFixture(fDef);
		body.applyForceToCenter(100, 100, true);

		light = new PointLight(GameState.rayHandler, 256, new Color(0.7f, 1, 1, 0.8f), 15, getPosition().x, getPosition().y);
		//light.setXray(true);
		light.attachToBody(body);
	}

	@Override
	public void tick(float dt) {
		if (health <= 0 && !isDead()) {
			SpaceLifeGame.getSoundManager().dieSound.play(0.32f);
			dead = true;
		}

		if (health < 10f) {
			health += dt;
		}

		if (isDead()) {
			return;
		}

		if (Gdx.input.isTouched()) {
			// Start with a magnitude 500 force to begin with
			Vector2 force = new Vector2(500 * dt * 60, 0);

			// get the mouse coords from the screen
			Vector3 pos = SpaceLifeGame.getVp().unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 1));

			// set the force angle to face the mouse coords
			force.setAngleRad((float) Math.atan2(pos.y - body.getPosition().y, pos.x - body.getPosition().x));

			// Move the Florp
			body.applyForceToCenter(force, true);

			float angle1 = force.angleRad();
			float angle2 = body.getAngle();

			// Fix angles to the 0...2PI range
			while (angle1 > Math.PI * 2f) {
				angle1 -= Math.PI * 2f;
			}

			while (angle1 < 0) {
				angle1 += Math.PI * 2f;
			}

			while (angle2 > Math.PI * 2f) {
				angle2 -= Math.PI * 2f;
			}

			while (angle2 < 0) {
				angle2 += Math.PI * 2f;
			}

			float angle;

			// Give the angular velocity depending on the difference
			// of angles wanted and have
			// angle1 = angle wanted
			// angle2 = angle have
			if (angle1 > angle2 + Math.PI) {
				angle = (angle1 - angle2) - (float) Math.PI * 2f;
			} else if (angle1 < angle2 - Math.PI) {
				angle = (angle1 - angle2) + (float) Math.PI * 2f;
			} else {
				angle = (angle1 - angle2);
			}

			// Set a max angular velocity
			if (angle > Math.PI / 4) {
				angle = (float) Math.PI / 4;
			} else if (angle < -Math.PI / 4) {
				angle = (float) -Math.PI / 4;
			}

			// Apply torque
			body.applyTorque(angle * 120f, true);
		}
	}

	@Override
	public void draw() {
		ShapeRenderer sr = GameState.getShapeRenderer();

		if (GameState.invincible) {
			sr.setColor(Color.GRAY);
			sr.circle(body.getPosition().x, body.getPosition().y, FLORP_RADIUS * 1.1f, 50);
		}

		Vector3 pos = SpaceLifeGame.getVp().unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 1));

		float mouseAngle = (float) Math.atan2(pos.y - body.getPosition().y, pos.x - body.getPosition().x);

		sr.setColor(bodyColor);
		sr.circle(body.getPosition().x, body.getPosition().y, FLORP_RADIUS, 50);

		float circle2rad = percentFull();
		if (circle2rad > 1.f) {
			circle2rad = 1.f;
		}

		sr.setColor(Color.GREEN);
		sr.circle(body.getPosition().x, body.getPosition().y, circle2rad * FLORP_RADIUS, 50);

		sr.setColor(Color.WHITE);

		sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() - Math.PI/5) * FLORP_RADIUS*0.65f,
				  body.getPosition().y + (float) Math.sin(body.getAngle() - Math.PI/5) * FLORP_RADIUS*0.65f, FLORP_RADIUS/4f, 25);

		sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() + Math.PI/5) * FLORP_RADIUS*0.65f,
				  body.getPosition().y + (float) Math.sin(body.getAngle() + Math.PI/5) * FLORP_RADIUS*0.65f, FLORP_RADIUS/4f, 25);

		sr.setColor(Color.BLACK);

		if (isDead()) {
			sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f,
					body.getPosition().y + (float) Math.sin(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f, FLORP_RADIUS / 4f, 15);

			sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f,
					body.getPosition().y + (float) Math.sin(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f, FLORP_RADIUS / 4f, 15);
		} else {
			float eyeAngle1 = (float) Math.atan2(pos.y - (body.getPosition().y + (float) Math.sin(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f),
					pos.x - (body.getPosition().x + (float) Math.cos(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f));
			float eyeAngle2 = (float) Math.atan2(pos.y - (body.getPosition().y + (float) Math.sin(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f),
					pos.x - (body.getPosition().x + (float) Math.cos(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f));

			sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f + (float) Math.cos(eyeAngle1) * (FLORP_RADIUS / 8f),
					body.getPosition().y + (float) Math.sin(body.getAngle() - Math.PI / 5) * FLORP_RADIUS * 0.65f + (float) Math.sin(eyeAngle1) * (FLORP_RADIUS / 8f), FLORP_RADIUS / 8f, 15);

			sr.circle(body.getPosition().x + (float) Math.cos(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f + (float) Math.cos(eyeAngle2) * (FLORP_RADIUS / 8f),
					body.getPosition().y + (float) Math.sin(body.getAngle() + Math.PI / 5) * FLORP_RADIUS * 0.65f + (float) Math.sin(eyeAngle2) * (FLORP_RADIUS / 8f), FLORP_RADIUS / 8f, 15);
		}
	}

	public boolean isDead() {
		return dead;
	}

	public void feed(float mass) {
		food += mass;

		Gdx.app.log("Player", "Ate: " + mass + ", Total: " + food);
	}

	public boolean isFull() {
		return (food >= 10);
	}

	public float percentFull() {
		return food / 10f;
	}

	public void damage(float damage) {
		health -= damage;
		if (health < 0) {
			health = 0;
		}
	}

	public float getHealth() {
		return health;
	}
}

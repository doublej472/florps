package com.gmail.doublej472.spacelife;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Array;
import com.gmail.doublej472.spacelife.entities.*;
import com.gmail.doublej472.spacelife.states.GameState;

/**
 * Created by doublej472 on 12/20/15.
 */
public class CollisionListener implements ContactListener {

	public static final Array<Entity> toRemove = new Array<Entity>();

	private PlayerEntity player;

	public CollisionListener(PlayerEntity player) {
		this.player = player;
	}

	@Override
	public void beginContact(Contact contact) {
		if ((contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity &&
				contact.getFixtureB().getBody().getUserData() instanceof FoodEntity) || (contact.getFixtureB().getBody().getUserData() instanceof PlayerEntity &&
				contact.getFixtureA().getBody().getUserData() instanceof FoodEntity)) {

			FoodEntity food;
			PlayerEntity player;

			if (contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity) {
				player = (PlayerEntity) contact.getFixtureA().getBody().getUserData();
				food = (FoodEntity) contact.getFixtureB().getBody().getUserData();
			} else {
				player = (PlayerEntity) contact.getFixtureB().getBody().getUserData();
				food = (FoodEntity) contact.getFixtureA().getBody().getUserData();
			}

			player.feed(food.getBody().getMass());

			SpaceLifeGame.getSoundManager().eatSound.play(food.getBody().getMass() / 3f);

			toRemove.add(food);

		}

		if ((contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity &&
				contact.getFixtureB().getBody().getUserData() instanceof TimePowerupEntity) || (contact.getFixtureB().getBody().getUserData() instanceof PlayerEntity &&
				contact.getFixtureA().getBody().getUserData() instanceof TimePowerupEntity)) {

			TimePowerupEntity time;
			PlayerEntity player;

			if (contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity) {
				player = (PlayerEntity) contact.getFixtureA().getBody().getUserData();
				time = (TimePowerupEntity) contact.getFixtureB().getBody().getUserData();
			} else {
				player = (PlayerEntity) contact.getFixtureB().getBody().getUserData();
				time = (TimePowerupEntity) contact.getFixtureA().getBody().getUserData();
			}

			SpaceLifeGame.getSoundManager().eatSound.play(0.8f);

			GameState.addTime(10f);

			toRemove.add(time);

		}

		if ((contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity &&
				contact.getFixtureB().getBody().getUserData() instanceof InvincibilityEntity) || (contact.getFixtureB().getBody().getUserData() instanceof PlayerEntity &&
				contact.getFixtureA().getBody().getUserData() instanceof InvincibilityEntity)) {

			InvincibilityEntity inv;
			PlayerEntity player;

			if (contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity) {
				player = (PlayerEntity) contact.getFixtureA().getBody().getUserData();
				inv = (InvincibilityEntity) contact.getFixtureB().getBody().getUserData();
			} else {
				player = (PlayerEntity) contact.getFixtureB().getBody().getUserData();
				inv = (InvincibilityEntity) contact.getFixtureA().getBody().getUserData();
			}

			SpaceLifeGame.getSoundManager().eatSound.play(0.8f);

			GameState.invincibleTimer = GameState.INVINCIBILITY_TIME;

			toRemove.add(inv);

		}

		if ((contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity &&
				contact.getFixtureB().getBody().getUserData() instanceof HomeRockEntity) || (contact.getFixtureB().getBody().getUserData() instanceof PlayerEntity &&
				contact.getFixtureA().getBody().getUserData() instanceof HomeRockEntity)) {

			PlayerEntity player;

			if (contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity) {
				player = (PlayerEntity) contact.getFixtureA().getBody().getUserData();
			} else {
				player = (PlayerEntity) contact.getFixtureB().getBody().getUserData();
			}

			if (player.isFull()) {
				GameState.setFinished();
			}

		}
	}

	@Override
	public void endContact(Contact contact) {

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

		if ((contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity && (contact.getFixtureB().getBody().getUserData() instanceof RockEntity || contact.getFixtureB().getBody().getUserData() instanceof HomeRockEntity)) ||
				(contact.getFixtureB().getBody().getUserData() instanceof PlayerEntity && (contact.getFixtureA().getBody().getUserData() instanceof RockEntity || contact.getFixtureA().getBody().getUserData() instanceof HomeRockEntity))) {

			float vol = 0;

			for (int i = 0; i < impulse.getCount(); i++) {
				vol += Math.abs(impulse.getNormalImpulses()[i]);
			}

			vol /= impulse.getCount();
			vol /= 600;

			if (vol < 0.025) {
				return;
			}

			SpaceLifeGame.getSoundManager().rockHitSound.play(vol);
		}

		if (contact.getFixtureA().getBody().getUserData() instanceof Entity && contact.getFixtureB().getBody().getUserData() instanceof Entity) {

			if (!(contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity || contact.getFixtureB().getBody().getUserData() instanceof PlayerEntity)) {
				float vol = 0;

				for (int i = 0; i < impulse.getCount(); i++) {
					vol += Math.abs(impulse.getNormalImpulses()[i]);
				}

				vol /= impulse.getCount();
				vol /= 1000;

				vol *= (1 / contact.getFixtureA().getBody().getPosition().dst(player.getPosition()) + 1 / contact.getFixtureB().getBody().getPosition().dst(player.getPosition())) / 2f;

				if (vol < 0.025) {
					return;
				}

				SpaceLifeGame.getSoundManager().rockHitSound.play(vol);
			}
		}

		if ((contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity &&
				(contact.getFixtureB().getBody().getUserData() instanceof MadFlorpEntity || contact.getFixtureB().getBody().getUserData() instanceof ChargerEntity)) || (contact.getFixtureB().getBody().getUserData() instanceof PlayerEntity &&
				(contact.getFixtureA().getBody().getUserData() instanceof MadFlorpEntity || contact.getFixtureA().getBody().getUserData() instanceof ChargerEntity))) {

			PlayerEntity player;

			if (contact.getFixtureA().getBody().getUserData() instanceof PlayerEntity) {
				player = (PlayerEntity) contact.getFixtureA().getBody().getUserData();
			} else {
				player = (PlayerEntity) contact.getFixtureB().getBody().getUserData();
			}

			float damage = 0;

			for (int i = 0; i < impulse.getCount(); i++) {
				damage += Math.abs(impulse.getNormalImpulses()[i]);
			}

			damage /= impulse.getCount();
			damage /= 400;
			damage += 0.5f;

			if (!GameState.invincible) {
				player.damage(damage);
			}

		}

	}
}

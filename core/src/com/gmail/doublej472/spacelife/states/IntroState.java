package com.gmail.doublej472.spacelife.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.gmail.doublej472.spacelife.SpaceLifeGame;

/**
 * Created by doublej472 on 12/19/15.
 */
public class IntroState extends State {
	// FADE_TIME is the time each fade (in or out) will last.
	// HOLD_TIME is the time each intro card will stay at 1.0 alpha (fully opaque).
	private static final float FADE_TIME = 1.0f, HOLD_TIME = 5.0f;
	private static final int FONT_SIZE = 64;
	private final String text = "Made Using";
	private final String text1 = "by DoubleJ472";
	// Used for calculating the font width.
	private final GlyphLayout layout;
	private final GlyphLayout layout1;
	SpriteBatch batch;
	Sprite img;
	BitmapFont font;
	private float alpha = 0.0f;
	private float backAlpha = 1.0f;
	private float timer = 0.0f;
	// Used to determine which part of the intro we are in.
	private int state = 0;

	public IntroState() {
		super();

		batch = new SpriteBatch();
		batch.enableBlending();

		img = new Sprite(new Texture("gdxlogo.png"));

		img.setPosition(Gdx.graphics.getWidth()/2 - img.getWidth()/2,
						Gdx.graphics.getHeight()/2 - img.getHeight()/2);

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = FONT_SIZE;
		parameter.color = Color.BLACK;
		font = generator.generateFont(parameter);
		generator.dispose();

		layout = new GlyphLayout(font, text);
		layout1 = new GlyphLayout(font, text1);
	}

	@Override
	public void tick(float dt) {
		if (Gdx.input.isTouched()) {
			state++;
			timer = 0;
		}

		switch (state) {
			case 0:
				timer += dt;
				alpha = timer/FADE_TIME;
				if (timer > FADE_TIME) {
					timer = 0;
					state++;
				}
				break;
			case 1:
				timer += dt;
				alpha = 1;
				if (timer > HOLD_TIME) {
					timer = 0;
					state++;
				}
				break;
			case 2:
				timer += dt;
				alpha = (FADE_TIME - timer)/FADE_TIME;
				backAlpha = (FADE_TIME - timer)/FADE_TIME;
				if (timer > FADE_TIME) {
					timer = 0;
					state++;
					alpha = 0;
				}
				break;
			default:
				alpha = 0;
				SpaceLifeGame.switchState(new MenuState());
		}
	}

	@Override
	public void draw() {
		Gdx.gl.glClearColor(backAlpha, backAlpha, backAlpha, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

		getViewPort().apply();

		batch.begin();
		font.setColor(1, 1, 1, alpha);
		font.draw(batch, text, (getViewPort().getScreenWidth() - layout.width) / 2, getViewPort().getScreenHeight() / 2 + FONT_SIZE * 2.5f);
		font.draw(batch, text1, (getViewPort().getScreenWidth() - layout1.width) / 2, getViewPort().getScreenHeight() / 2 - FONT_SIZE * 2.5f);

		img.draw(batch, alpha);
		batch.end();
	}

	@Override
	public void dispose() {
		batch.dispose();
		font.dispose();
	}
}

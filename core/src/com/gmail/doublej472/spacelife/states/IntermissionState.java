package com.gmail.doublej472.spacelife.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.gmail.doublej472.spacelife.SpaceLifeGame;

/**
 * Created by doublej472 on 12/21/15.
 */
public class IntermissionState extends State {
	private static final int FONT_SIZE = 48;
	private static final int LEVEL_FONT_SIZE = 64;
	private static final int SMALL_FONT = 18;

	private float timeLimit;
	private float radius;
	private int foodAmount;
	private int rockAmount;
	private int homeRockAmount;
	private int madFlorpAmount;
	private int chargerAmount;
	private int powerupAmount;

	private SpriteBatch sb = new SpriteBatch();

	private BitmapFont menuFont;
	private BitmapFont levelFont;
	private BitmapFont smallFont;
	private BitmapFont hostileFont;

	private Stage stage = new Stage();
	private VerticalGroup group = new VerticalGroup();
	private TextButton.TextButtonStyle textStyle = new TextButton.TextButtonStyle();
	private TextButton startButton;
	private TextButton backButton;

	public IntermissionState() {
		super();

		radius = (float) Math.random() * 20f + 40f;
		foodAmount = (int) (4f / (SpaceLifeGame.getLevel() + 5) * 25f) + 10;
		rockAmount = (int) (2 * (Math.log(SpaceLifeGame.getLevel()) / Math.log(5)) + 6);
		homeRockAmount = (int) ((radius / (50f / 2f)));
		madFlorpAmount = SpaceLifeGame.getLevel() / 2;
		chargerAmount = (SpaceLifeGame.getLevel() - 1) / 3;
		timeLimit = 150f / (SpaceLifeGame.getLevel() / 10f + 1);
		powerupAmount = 2 / (SpaceLifeGame.getLevel() / 2 + 1) + 2;

		// Generate font for the menu
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = FONT_SIZE;
		parameter.color = com.badlogic.gdx.graphics.Color.WHITE;
		menuFont = generator.generateFont(parameter);

		// Generate font for the level indicator
		parameter.size = LEVEL_FONT_SIZE;
		parameter.color = com.badlogic.gdx.graphics.Color.CYAN;
		levelFont = generator.generateFont(parameter);

		// Generate font for the info text
		parameter.size = SMALL_FONT;
		parameter.color = com.badlogic.gdx.graphics.Color.WHITE;
		smallFont = generator.generateFont(parameter);

		// Generate font for the info text
		parameter.size = SMALL_FONT;
		parameter.color = com.badlogic.gdx.graphics.Color.RED;
		hostileFont = generator.generateFont(parameter);
		generator.dispose();

		group.setFillParent(true);

		textStyle.font = menuFont;

		startButton = new TextButton("Start!", textStyle);
		backButton = new TextButton("Back", textStyle);

		Label.LabelStyle lStyle = new Label.LabelStyle();
		lStyle.font = levelFont;

		Label level = new Label("Level " + SpaceLifeGame.getLevel(), lStyle);

		Label.LabelStyle lStyleSmall = new Label.LabelStyle();
		lStyleSmall.font = smallFont;

		Label.LabelStyle lStyleHostile = new Label.LabelStyle();
		lStyleHostile.font = hostileFont;

		String size;
		if (radius < 45f) {
			size = "small";
		} else if (radius < 55f) {
			size = "medium";
		} else {
			size = "large";
		}

		Label info = new Label("Level has " + foodAmount + " food, " + rockAmount + " rocks, and is " + size + " size.", lStyleSmall);
		Label info2 = new Label("You have " + (int) timeLimit + " seconds to finish the level.", lStyleSmall);

		String dangerText = "";

		if (madFlorpAmount > 1) {
			dangerText += ("There are " + madFlorpAmount + " Mad Florps");
		} else if (madFlorpAmount == 1) {
			dangerText += ("There is " + madFlorpAmount + " Mad Florp");
		}

		if (chargerAmount > 1) {
			dangerText += (" and " + chargerAmount + " Chargers");
		} else if (chargerAmount == 1) {
			dangerText += (" and " + chargerAmount + " Charger");
		}

		if (!dangerText.equals("")) {
			dangerText += (", be careful!");
		}

		Label danger = new Label(dangerText, lStyleHostile);

		stage.addActor(group);

		//table.setDebug(true);
		group.center();
		group.pad(5f);

		group.addActor(level);
		group.addActor(info);
		group.addActor(info2);
		group.addActor(danger);
		group.addActor(startButton);
		group.addActor(backButton);

		Gdx.input.setInputProcessor(stage);

		SpaceLifeGame.getVp().apply();

	}

	@Override
	public void tick(float dt) {
		stage.act(dt);

		if (startButton.isPressed()) {
			beginGame();
		}

		// go back to menu game.
		if (backButton.isPressed()) {
			SpaceLifeGame.switchState(new MenuState());
		}
	}

	private void beginGame() {

		Gdx.app.log("Intermission", "Level has " + foodAmount + " food, " + rockAmount + " rocks, " + homeRockAmount + " home rocks, and is " + radius + " big.");
		SpaceLifeGame.switchState(new GameState(radius, foodAmount, rockAmount, homeRockAmount, timeLimit, madFlorpAmount, chargerAmount, powerupAmount));
	}

	@Override
	public void draw() {
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		menuFont.dispose();
		sb.dispose();
		smallFont.dispose();
		levelFont.dispose();
	}
}

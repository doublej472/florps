package com.gmail.doublej472.spacelife.states;

import box2dLight.RayHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.gmail.doublej472.spacelife.CollisionListener;
import com.gmail.doublej472.spacelife.EntityManager;
import com.gmail.doublej472.spacelife.SpaceLifeGame;
import com.gmail.doublej472.spacelife.entities.HomeRockEntity;
import com.gmail.doublej472.spacelife.entities.PlayerEntity;

/**
 * Created by doublej472 on 12/19/15.
 *
 * This is the main game, and it uses too many static variables.
 */
public class GameState extends State {
	public static final float INVINCIBILITY_TIME = 5f;
	private static final float TRANSITION_MAX = 3f;
	public static boolean invincible = true;
	public static float invincibleTimer = 0f;
	public static RayHandler rayHandler;
	private static World world;
	private static ShapeRenderer sr = new ShapeRenderer();
	private static SpriteBatch sb = new SpriteBatch();
	private static SpriteBatch guisb = new SpriteBatch();
	private static Box2DDebugRenderer debug = new Box2DDebugRenderer();
	private static boolean finished;
	private static float timeLeft;
	private static EntityManager em;
	private GlyphLayout layout;
	private BitmapFont font;
	private PlayerEntity player;
	private Sprite space = new Sprite(new Texture("space.png"));
	private Sprite space2 = new Sprite(new Texture("space2.png"));
	private Sprite space3 = new Sprite(new Texture("space3.png"));
	private Sprite noise = new Sprite(new Texture("noise.png"));
	private long soundId;
	private float radius = 0f;
	private int maxFood = 0;
	private int maxPowerups = 0;
	private float accumulator = 0;
	private float transitionTime = TRANSITION_MAX;
	private float zoomScale = 1f;
	private float targetRotate = 0f;

	public GameState(float radius, int foodAmount, int rockAmount, int homeRockAmount, float timeLimit, int madFlorpAmount, int chargerAmount, int powerupCount) {
		super();

		finished = false;

		soundId = SpaceLifeGame.getSoundManager().staticSound.loop(0.0f);

		this.radius = radius;
		this.maxFood = foodAmount;
		this.maxPowerups = powerupCount;
		timeLeft = timeLimit;

		em = new EntityManager();

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = 32;
		parameter.color = Color.WHITE;
		parameter.borderColor = Color.BLACK;
		parameter.borderWidth = 2;
		font = generator.generateFont(parameter);
		generator.dispose();

		layout = new GlyphLayout(font, "");

		world = new World(new Vector2(0, 0), true);

		RayHandler.setGammaCorrection(true);
		RayHandler.useDiffuseLight(true);

		rayHandler = new RayHandler(world);
		rayHandler.setAmbientLight(0.01f, 0.01f, 0.01f, 1f);
		rayHandler.setBlurNum(2);
		rayHandler.setShadows(true);

		for (int i = 0; i < foodAmount; i++) {
			Vector2 pos = new Vector2(0, 1);
			pos.setLength((float) (radius*(Math.random()*0.85 + 0.15)));
			pos.rotateRad((float) (2*Math.PI*Math.random()));
			em.newFood(pos);
		}

		for (int i = 0; i < powerupCount; i++) {
			Vector2 pos = new Vector2(0, 1);
			pos.setLength((float) (radius*Math.random()));
			pos.rotateRad((float) (2*Math.PI*Math.random()));
			if (Math.random() > 0.5) {
				em.newTimePowerup(pos);
			} else {
				em.newInvincibilityPowerup(pos);
			}
		}

		for (int i = 0; i < rockAmount; i++) {
			Vector2 pos = new Vector2(0, 1);
			pos.setLength((float) (radius*Math.random()));
			pos.rotateRad((float) (2*Math.PI*Math.random()));
			em.newRock(pos);
		}

		for (int i = 1; i < homeRockAmount; i++) {
			Vector2 pos = new Vector2(0, 1);
			pos.setLength((float) (radius*Math.random()));
			pos.rotateRad((float) (2*Math.PI*Math.random()));
			em.newHomeRock(pos);
		}

		em.newHomeRock(new Vector2(0, 0));

		// setup the space textures
		float SPACE_SIZE = 30;

		space.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		space2.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		space3.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

		//space.setSize(SPACE_SIZE, SPACE_SIZE);
		space.setSize(SPACE_SIZE / 2f * 1920 / 1080, SPACE_SIZE / 2f);
		space2.setSize(SPACE_SIZE, SPACE_SIZE);
		space3.setSize(SPACE_SIZE, SPACE_SIZE);

		noise.setSize(Gdx.graphics.getWidth()*2, Gdx.graphics.getWidth()*2);
		noise.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

		player = new PlayerEntity(new Vector2());

		for (int i = 0; i < madFlorpAmount; i++) {
			// Add mad florps to the outside the the map.
			Vector2 pos = new Vector2(0, radius);
			pos.rotateRad((float) (Math.PI * 2 * Math.random()));
			em.newMadFlorp(pos, player);
		}

		for (int i = 0; i < chargerAmount; i++) {
			// Add mad florps to the outside the the map.
			Vector2 pos = new Vector2(0, radius * 1.2f);
			pos.rotateRad((float) (Math.PI * 2 * Math.random()));
			em.newCharger(pos, player);
		}

		fixShapes(800);

		world.setContactListener(new CollisionListener(player));

		// Update Camera to current player position
		getCamera().position.set(player.getPosition().x, player.getPosition().y, getCamera().position.z);
		getCamera().update();
	}

	public static ShapeRenderer getShapeRenderer() {
		return sr;
	}

	public static World getWorld() {
		return world;
	}

	public static OrthographicCamera getCamera() {
		return cam;
	}

	public static void setFinished() {
		finished = true;
	}

	public static void addTime(float time) {
		timeLeft += time;
	}

	public static EntityManager getEntityManager() {
		return em;
	}

	/**
	 * Simulates the box2d world for a while to fix any issues with overlapping shapes.
	 */
	private void fixShapes(int iterations) {

		// Simulate the world for a while
		for (int i = 0; i < iterations; i++) {
			world.step(1 / 60f, 4, 2);
		}

	}

	@Override
	public void tick(float dt) {
		if (player.isDead()) {
			finished = true;
			SpaceLifeGame.setLevel(0);
		}

		if (finished) {
			transitionTime -= dt;
			if (!player.isDead()) {
				em.killEnemies();
			}
		} else {
			timeLeft -= dt;
		}

		if (transitionTime < 0f) {
			SpaceLifeGame.setLevel(SpaceLifeGame.getLevel() + 1);
			SpaceLifeGame.switchState(new IntermissionState());
			return;
		}

		if (timeLeft < 0) {
			player.damage(100f);
		}

		if (invincibleTimer > 0) {
			invincible = true;
			invincibleTimer -= dt;
		} else {
			invincible = false;
		}

		player.tick(dt);
		em.tick(dt);

		if (em.foodNumber() < maxFood) {
			Vector2 pos = new Vector2(0, 1);
			pos.setLength((float) (radius*Math.random()));
			pos.rotateRad((float) (2*Math.PI*Math.random()));
			em.newFood(pos);
		}

		if (em.powerupNumber() < maxPowerups) {
			Vector2 pos = new Vector2(0, 1);
			pos.setLength((float) (radius*Math.random()));
			pos.rotateRad((float) (2*Math.PI*Math.random()));
			if (Math.random() > 0.5) {
				em.newTimePowerup(pos);
			} else {
				em.newInvincibilityPowerup(pos);
			}
		}

		float lerp = 3 * dt;

		Vector3 position = getCamera().position;
		position.x += (player.getPosition().x + player.getBody().getLinearVelocity().x * 0.4f - position.x) * lerp;
		position.y += (player.getPosition().y + player.getBody().getLinearVelocity().y * 0.4f - position.y) * lerp;

		float zoomLerp = 2 * dt;

		if (Gdx.input.isKeyPressed(Input.Keys.J)) {
			zoomScale -= 0.1f;
		}

		if (Gdx.input.isKeyPressed(Input.Keys.K)) {
			zoomScale += 0.1f;
		}

		if (Gdx.input.isKeyPressed(Input.Keys.A)) {
			targetRotate -= 0.1;
		}

		if (Gdx.input.isKeyPressed(Input.Keys.D)) {
			targetRotate += 0.1;
		}

		getCamera().zoom += (zoomScale + 0.06f * (player.getBody().getLinearVelocity().len()) - getCamera().zoom) * zoomLerp;
		getCamera().rotate(targetRotate);
		targetRotate *= 0.9 * 60 * dt;

		cam.update();

		doPhysicsStep(dt);
	}

	private void doPhysicsStep(float deltaTime) {
		// fixed time step
		// max frame time to avoid spiral of death (on slow devices)
		float frameTime = Math.min(deltaTime, 0.25f);
		accumulator += frameTime;
		while (accumulator >= 1 / 60f) {
			world.step(1 / 60f, 6, 4);
			accumulator -= 1 / 60f;
		}
		world.clearForces();
	}

	@Override
	public void draw() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		float xOffset = -cam.position.x * 0.1f;
		float yOffset = -cam.position.y * 0.1f;
		float xOffset2 = -cam.position.x * 0.15f;
		float yOffset2 = -cam.position.y * 0.15f;
		float xOffset3 = -cam.position.x * 0.2f;
		float yOffset3 = -cam.position.y * 0.2f;

		// Set up the parallax offsets
		while (xOffset > space.getWidth()) {
			xOffset -= space.getWidth();
		}
		while (yOffset > space.getHeight()) {
			yOffset -= space.getHeight();
		}

		while (xOffset2 < -space2.getWidth()) {
			xOffset2 += space2.getWidth();
		}
		while (yOffset2 < -space2.getHeight()) {
			yOffset2 += space2.getHeight();
		}

		while (xOffset3 < -space3.getWidth()) {
			xOffset3 += space3.getWidth();
		}
		while (yOffset3 < -space3.getHeight()) {
			yOffset3 += space3.getHeight();
		}

		// Draw the parallax background for space.
		sb.setProjectionMatrix(cam.combined);
		sb.begin();
		for (int i = (int) (-(getCamera().viewportWidth * getCamera().zoom) / (space.getWidth())) - 1; (i - 1) * space.getWidth() + xOffset < getCamera().viewportWidth * getCamera().zoom; i++) {
			for (int j = (int) (-(getCamera().viewportHeight * getCamera().zoom) / (space.getHeight())) - 1; (j - 1) * space.getHeight() + yOffset < getCamera().viewportHeight * getCamera().zoom; j++) {
				space.setPosition(getCamera().position.x - getCamera().viewportWidth / 2 + i * space.getWidth() + xOffset,
						getCamera().position.y - getCamera().viewportHeight / 2 + j * space.getHeight() + yOffset);
				space.draw(sb);
			}
		}
		for (int i = (int) (-(getCamera().viewportWidth * getCamera().zoom) / (space2.getWidth())) - 1; (i - 1) * space2.getWidth() + xOffset2 < getCamera().viewportWidth * getCamera().zoom; i++) {
			for (int j = (int) (-(getCamera().viewportHeight * getCamera().zoom) / (space2.getHeight())) - 1; (j - 1) * space2.getHeight() + yOffset2 < getCamera().viewportHeight * getCamera().zoom; j++) {
				space2.setPosition(getCamera().position.x - getCamera().viewportWidth / 2 + i * space2.getWidth() + xOffset2,
						getCamera().position.y - getCamera().viewportHeight / 2 + j * space2.getHeight() + yOffset2);
				space2.draw(sb);
			}
		}
		for (int i = (int) (-(getCamera().viewportWidth * getCamera().zoom) / (space3.getWidth())) - 1; (i - 1) * space3.getWidth() + xOffset3 < getCamera().viewportWidth * getCamera().zoom; i++) {
			for (int j = (int) (-(getCamera().viewportHeight * getCamera().zoom) / (space3.getHeight())) - 1; (j - 1) * space3.getHeight() + yOffset3 < getCamera().viewportHeight * getCamera().zoom; j++) {
				space3.setPosition(getCamera().position.x - getCamera().viewportWidth / 2 + i * space3.getWidth() + xOffset3,
						getCamera().position.y - getCamera().viewportHeight / 2 + j * space3.getHeight() + yOffset3);
				space3.draw(sb);
			}
		}
		sb.end();

		// Draw player and entities
		sr.setProjectionMatrix(cam.combined);
		sr.begin(ShapeRenderer.ShapeType.Filled);
		em.draw();
		player.draw();
		sr.end();

		rayHandler.setCombinedMatrix(cam);
		if (SpaceLifeGame.isHq()) {
			rayHandler.updateAndRender();
		}

		// Draw arrow home if the player is full
		if (player.isFull()) {
			HomeRockEntity h = em.getClosestHome(player.getPosition());

			float angleHome = (float) Math.atan2(h.getPosition().y - player.getPosition().y, h.getPosition().x - player.getPosition().x);

			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
			sr.setProjectionMatrix(cam.combined);
			sr.begin(ShapeRenderer.ShapeType.Filled);

			sr.setColor(0, 1, 1, Math.max(0, Math.min((player.getPosition().dst(h.getPosition()) - 10f) / 20f, 1f)));

			sr.triangle(player.getPosition().x + (float) Math.cos(angleHome) * 5, player.getPosition().y + (float) Math.sin(angleHome) * 5,
					player.getPosition().x + (float) Math.cos(angleHome + 0.2f) * 4.5f, player.getPosition().y + (float) Math.sin(angleHome + 0.2f) * 4.5f,
					player.getPosition().x + (float) Math.cos(angleHome - 0.2f) * 4.5f, player.getPosition().y + (float) Math.sin(angleHome - 0.2f) * 4.5f);

			sr.end();
		}

		guisb.begin();

		String text;

		if (player.percentFull() >= 1) {
			text = "Get Back Home!";
		} else if (player.percentFull() > 0) {
			text = Integer.toString((int) (player.percentFull() * 100)) + "%";
		} else {
			text = "Eat More Food!";
		}

		layout.setText(font, text);
		font.draw(guisb, text, (Gdx.graphics.getWidth() - layout.width) / 2f,
				(Gdx.graphics.getHeight() + Gdx.graphics.getHeight() / 2f + layout.height) / 2f);

		String timeLeftString = Integer.toString((int) timeLeft) + " Seconds Remaining";

		layout.setText(font, timeLeftString);

		if (timeLeft < 30 && timeLeft >= 10) {
			font.setColor(1f, 0.6f, 0.6f, 1);
		} else if (timeLeft < 10) {
			font.setColor(Color.RED);
		} else {
			font.setColor(Color.WHITE);
		}

		if (timeLeft > 0) {
			font.draw(guisb, timeLeftString, (Gdx.graphics.getWidth() - layout.width) / 2f,
					(Gdx.graphics.getHeight() + 2f * Gdx.graphics.getHeight() / 3f + layout.height) / 2f);
		}

		if (player.getHealth() < 10f) {
			noise.setPosition((float) (Math.random() * (Gdx.graphics.getWidth() - noise.getWidth())), (float) (Math.random() * (Gdx.graphics.getHeight() - noise.getHeight())));
			noise.draw(guisb, (10f - player.getHealth())/10f);
			SpaceLifeGame.getSoundManager().staticSound.setVolume(soundId, (10f - player.getHealth()) / 10f);
		} else {
			SpaceLifeGame.getSoundManager().staticSound.setVolume(soundId, 0f);
		}

		if (finished) {
			SpaceLifeGame.getSoundManager().staticSound.setVolume(soundId, 0f);
		}

		guisb.end();

		//debug.render(world, cam.combined);

		if (transitionTime < TRANSITION_MAX) {
			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

			sr.setProjectionMatrix(cam.combined);
			sr.begin(ShapeRenderer.ShapeType.Filled);
			sr.setColor(0, 0, 0, (TRANSITION_MAX-transitionTime)/TRANSITION_MAX);
			//sr.setColor(0, 0, 0, 0.5f);
			sr.rect((getCamera().position.x - (getCamera().viewportWidth*getCamera().zoom/2f)), (getCamera().position.y - (getCamera().viewportHeight*getCamera().zoom/2f)),
					(getCamera().viewportWidth*getCamera().zoom), (getCamera().viewportHeight*getCamera().zoom));
			sr.end();
		}
	}

	@Override
	public void dispose() {
		//SpaceLifeGame.getSoundManager().music.stop();
		//SpaceLifeGame.getSoundManager().music.setPosition(0);
		world.dispose();
		rayHandler.dispose();
	}
}

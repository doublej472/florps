package com.gmail.doublej472.spacelife.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gmail.doublej472.spacelife.SpaceLifeGame;

/**
 * Created by doublej472 on 12/18/15.
 */
public abstract class State {

	protected static OrthographicCamera cam;

	public State() {
		cam = (OrthographicCamera) SpaceLifeGame.getVp().getCamera();
		SpaceLifeGame.getVp().apply();
	}

	/**
	 * Ticks the state by dt seconds
	 * @param dt The time in seconds from the last frame.
	 */
	public abstract void tick(float dt);

	/**
	 * Draws the state.
	 * State should have its own rendering method.
	 */
	public abstract void draw();

	/**
	 * Dispose the state. Check your references.
	 * Should dispose everything required by LibGDX.
	 */
	public abstract void dispose();

	public Viewport getViewPort() {
		return SpaceLifeGame.getVp();
	}
}

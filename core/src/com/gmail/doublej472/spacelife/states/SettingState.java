package com.gmail.doublej472.spacelife.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.gmail.doublej472.spacelife.SpaceLifeGame;

/**
 * This is the Main Menu for this game.
 * Nothing really special here.
 * <p/>
 * Created by doublej472 on 12/19/15.
 */
public class SettingState extends State {
	private static final int FONT_SIZE = 56;
	private SpriteBatch sb = new SpriteBatch();
	private BitmapFont menuFont;
	private Stage stage = new Stage();
	private Table table = new Table();
	private TextButton.TextButtonStyle textStyle = new TextButton.TextButtonStyle();
	private TextButton hqButton;
	private TextButton musicButton;
	private TextButton backButton;

	public SettingState() {

		super();

		// Generate font for the menu
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = FONT_SIZE;
		parameter.color = com.badlogic.gdx.graphics.Color.WHITE;
		parameter.borderColor = com.badlogic.gdx.graphics.Color.BLACK;
		parameter.borderWidth = 3;
		menuFont = generator.generateFont(parameter);
		generator.dispose();

		table.setFillParent(true);
		//stage.getCamera().position.set(0, 0, 1);

		textStyle.font = menuFont;

		hqButton = new TextButton("HQ: " + (SpaceLifeGame.isHq() ? "On" : "Off"), textStyle);
		backButton = new TextButton("Back", textStyle);
		musicButton = new TextButton("Music: " + (SpaceLifeGame.getSoundManager().music.getVolume() > 0 ? "On" : "Off"), textStyle);

		hqButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SpaceLifeGame.setHq(!SpaceLifeGame.isHq());
				hqButton.setText("HQ: " + (SpaceLifeGame.isHq() ? "On" : "Off"));
			}
		});

		musicButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (!SpaceLifeGame.isMusicOn()) {
					SpaceLifeGame.setMusicOn(true);
					musicButton.setText("Music: On");
				} else {
					SpaceLifeGame.setMusicOn(false);
					musicButton.setText("Music: Off");
				}
			}
		});

		backButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				SpaceLifeGame.switchState(new MenuState());
			}
		});

		stage.addActor(table);

		//table.setDebug(true);
		table.pad(10f);

		table.center().add(hqButton);
		table.row();
		table.center().add(musicButton);
		table.row();
		table.center().add(backButton);

		//space.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		//title.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

		Gdx.input.setInputProcessor(stage);

		SpaceLifeGame.getVp().apply();

	}

	@Override
	public void tick(float dt) {
		stage.act(dt);
	}

	@Override
	public void draw() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		getViewPort().apply();

		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		menuFont.dispose();
		sb.dispose();
	}
}

package com.gmail.doublej472.spacelife.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.gmail.doublej472.spacelife.SpaceLifeGame;

/**
 * This is the Main Menu for this game.
 * Nothing really special here.
 *
 * Created by doublej472 on 12/19/15.
 */
public class MenuState extends State {
	private static final int FONT_SIZE = 56;
	private Sprite space = new Sprite(new Texture("space.png"));
	private float offsetX = 0;
	private SpriteBatch sb = new SpriteBatch();
	private BitmapFont menuFont;
	private BitmapFont smallFont;
	private Sprite title;
	private Stage stage = new Stage();
	private Table table = new Table();
	private TextButton.TextButtonStyle textStyle = new TextButton.TextButtonStyle();
	private TextButton playButton;
	private TextButton quitButton;
	private TextButton settingsButton;

	public MenuState() {

		super();

		// Load the title sprite
		title = new Sprite(new Texture("title.png"));
		title.setOriginCenter();
		title.setScale(0.3f);
		//title.setPosition((Gdx.graphics.getWidth() - title.getWidth()) / 2f, -50);

		// Generate font for the menu
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = FONT_SIZE;
		parameter.color = com.badlogic.gdx.graphics.Color.WHITE;
		parameter.borderColor = com.badlogic.gdx.graphics.Color.BLACK;
		parameter.borderWidth = 3;
		menuFont = generator.generateFont(parameter);

		parameter.size = 24;
		parameter.color = com.badlogic.gdx.graphics.Color.WHITE;
		parameter.borderColor = com.badlogic.gdx.graphics.Color.BLACK;
		parameter.borderWidth = 1;
		smallFont = generator.generateFont(parameter);
		generator.dispose();

		table.setFillParent(true);
		//stage.getCamera().position.set(0, 0, 1);

		textStyle.font = menuFont;

		playButton = new TextButton("Play Game", textStyle);
		settingsButton = new TextButton("Settings", textStyle);
		quitButton = new TextButton("Quit", textStyle);

		stage.addActor(table);

		//table.setDebug(true);
		table.pad(10f);

		table.center().add(playButton);
		table.row();
		table.center().add(settingsButton);
		table.row();
		table.center().add(quitButton);

		table.pack();
		table.setY(-80);

		title.setPosition((Gdx.graphics.getWidth() - title.getWidth()) / 2f, (Gdx.graphics.getHeight() - title.getHeight()) / 2f + 160);

		space.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
		title.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

		Gdx.input.setInputProcessor(stage);

		SpaceLifeGame.getVp().apply();

	}

	@Override
	public void tick(float dt) {
		stage.act(dt);

		offsetX -= dt*10f;
		if (offsetX < -space.getWidth()) {
			offsetX += space.getWidth();
		}

		space.setPosition(offsetX, 0);

		if (playButton.isPressed()) {
			//game.switchState(new GameState(50f, 150, 20));
			SpaceLifeGame.switchState(new IntermissionState());
		}

		if (settingsButton.isPressed()) {
			SpaceLifeGame.switchState(new SettingState());
		}

		// Quit game.
		if (quitButton.isPressed()) {
			Gdx.app.exit();
		}
	}

	@Override
	public void draw() {
		getViewPort().apply();

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		sb.begin();
		for (int i = 0; i * space.getWidth() + offsetX < Gdx.graphics.getWidth(); i++) {
			space.setPosition(offsetX + i*space.getWidth(), 0);
			space.draw(sb);
		}
		title.draw(sb);
		smallFont.draw(sb, "Music: \"Shaken\" by Riot", 5, smallFont.getLineHeight());
		sb.end();

		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		menuFont.dispose();
		sb.dispose();
	}
}

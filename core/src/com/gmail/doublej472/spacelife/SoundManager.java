package com.gmail.doublej472.spacelife;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by doublej472 on 12/21/15.
 */
public class SoundManager {
	public Sound dieSound;
	public Sound rockHitSound;
	public Sound staticSound;
	public Sound eatSound;

	public Music music;

	public SoundManager() {
		dieSound = Gdx.audio.newSound(Gdx.files.internal("dead.wav"));
		rockHitSound = Gdx.audio.newSound(Gdx.files.internal("hitRock.wav"));
		staticSound = Gdx.audio.newSound(Gdx.files.internal("noise.wav"));
		eatSound = Gdx.audio.newSound(Gdx.files.internal("eat.wav"));

		music = Gdx.audio.newMusic(Gdx.files.internal("shaken.mp3"));
		music.setVolume(0.31f);
		music.setLooping(true);
		music.play();
	}

	public void dispose() {
		dieSound.dispose();
		rockHitSound.dispose();
		staticSound.dispose();
		eatSound.dispose();

		music.dispose();
	}
}
